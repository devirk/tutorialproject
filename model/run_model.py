import numpy as np # used for handling numbers
import pandas as pd # used for handling the dataset
from sklearn.model_selection import train_test_split # used for splitting training and testing data
from sklearn.preprocessing import StandardScaler # used for feature scaling
from IPython.display import display 
import sqlalchemy
import pymysql
from sklearn.model_selection import train_test_split 
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LogisticRegression
import os
import sys
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
import mysql.connector as msql
from mysql.connector import Error
import csv

class Model:
    def run_datamodel(self):
        engine2 = create_engine('mysql+pymysql://root:@localhost:3306/db', echo=True)
        #Read from db - Train
        sql= "SELECT * FROM train"
        df_train = pd.read_sql(sql, con=engine2)
        print(df_train)

        #Read from db - Test
        sql= "SELECT * FROM test"
        df_test = pd.read_sql(sql, con=engine2)
        print(df_test)

        print(df_train.iloc[:,1])

        X_train=df_train.iloc[:,2:9]
        #print(X_train)
        y_train=df_train.iloc[:,1]
        X_test=df_test.iloc[:,2:9]
        y_test=df_test.iloc[:,1]

        #Normalization of features
        scaler = StandardScaler()
        X_train = pd.DataFrame(scaler.fit_transform(X_train), columns = X_train.columns)
        X_test = pd.DataFrame(scaler.transform(X_test), columns = X_test.columns)


        #Run a model on the preprocessed 
        log_reg = LogisticRegression(penalty = 'l2', dual = False, tol = 1e-4, fit_intercept = True, 
                            solver = 'liblinear')
        log_reg.fit(X_train, y_train)
        y_true, y_pred = y_test,log_reg.predict(X_test)
        print(classification_report(y_true, y_pred))
        print ("Accuracy:" ,accuracy_score(y_true, y_pred))