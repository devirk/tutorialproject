# A modular python project

An ML model is run on the Titanic dataset with main.py calling the module on data_preprocess.py for performing the preprocessing of the dataset and then storing it in mysql database and calling the module in data_model.py for fetching the stored data from mysql and run the logistic regression model on the data to get the output. The output gives us the accuracy of the prediction done by the model. 

## How to run the project

python main.py