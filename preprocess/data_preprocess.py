import numpy as np # used for handling numbers
import pandas as pd # used for handling the dataset
from IPython.display import display 
import sqlalchemy
import pymysql
from sklearn.model_selection import train_test_split 
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import os
import sys
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError
import mysql.connector as msql
from mysql.connector import Error
import csv

class Preprocess:
    def data_prep(self):
        #Reading dataset
        d_train = pd.read_csv('/home/devi/Tutorialpro/preprocess/train.csv')
        #d_test = pd.read_csv('test.csv')


        # split the data into train and test set
        d_train, d_test = train_test_split(d_train, test_size=0.2, random_state=42, shuffle=True)

        #PREPROCESSING train data
        print(d_train.columns)

        column_names = d_train.columns
        for column in column_names:
            print(column + ' - ' + str(d_train[column].isnull().sum()))

        #unbalanced class
        print(d_train.Survived.value_counts())

        #drop columns
        d_train = d_train.drop(columns=['Name','Ticket', 'PassengerId', 'Cabin'])

        #mapping embarked and sex to numerical values
        d_train['Sex'] = d_train['Sex'].map({'male':0, 'female':1})
        d_train['Embarked'] = d_train['Embarked'].map({'C':0, 'Q':1, 'S':2})

        #replace empty with nan
        d_train = d_train.replace(r'^\s+$', np.nan, regex=True)
        display(d_train)

        #drop rows with nan
        d_train.dropna(how='any',inplace=True)
        display(d_train)
        print(d_train.columns)

 
        #PREPROCESSING test data
        #drop columns
        d_test = d_test.drop(columns=['Name','Ticket', 'PassengerId', 'Cabin'])
        d_test['Sex'] = d_test['Sex'].map({'male':0, 'female':1})
        d_test['Embarked'] = d_test['Embarked'].map({'C':0, 'Q':1, 'S':2})

        #replace empty with nan
        d_test = d_test.replace(r'^\s+$', np.nan, regex=True)
        display(d_test)

        #drop rows with nan
        d_test.dropna(how='any',inplace=True)
        display(d_test)

        #Store preprocessed data into mysql db
        engine = create_engine('mysql+pymysql://root:@localhost:3306/db', echo=True)
        d_train.to_sql('train', con=engine,if_exists='replace')
        d_test.to_sql('test', con=engine,if_exists='replace')

        